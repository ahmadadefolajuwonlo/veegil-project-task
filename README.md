# Veegil project task



## Veegil Mobile Banking APP

A mock design of a simple banking application for both mobile and web browser screens.
Project include:
1. Users ability to signup and login into their account
2. User's phone number represent the account number
3. Users ability to deposit and send out money to others.
4. Users ability to view a visualization that shows deposit and withdrawal history.
5. Users ability to view the list of transactions, also to print out and share transaction.


## Project Design Link

- [ ] [Veegil Mobile Banking Design Task](https://www.figma.com/file/yhZbvd6oGdyzAwtG115emQ/Veegil-Project?type=design&node-id=0-1&mode=design&t=MgWkhj1eIvKb96No-0)